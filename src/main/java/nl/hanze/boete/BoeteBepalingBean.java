package nl.hanze.boete;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.ObjectMessage;
import javax.jms.Topic;

import nl.hanze.boete.dao.BoeteRegelDAO;
import nl.hanze.boete.dto.BoeteRegel;

/**
 * EJB voor het bepalen van de hoogte van een boete.
 * 
 * @author Johan Mulder
 */
@Stateless
public class BoeteBepalingBean
{
	@Inject
	private BoeteRegelDAO boeteRegelDAO;
	@Resource(lookup = "jms/boeteafhandeling")
	private Topic boeteAfhandelingTopic;
    @Inject
    private JMSContext context;


    /**
     * Benodigd voor injection in unit testing.
     * @param dao
     */
	void setBoeteRegelDAO(BoeteRegelDAO dao)
	{
		boeteRegelDAO = dao;
	}
	
	/**
	 * Benodigd voor injection in unit testing.
	 * @param context
	 */
	void setJMSContext(JMSContext context)
	{
		this.context = context;
	}

	/**
	 * Bereken de boete met de gegeven parameters.
	 * 
	 * @param params
	 * @return
	 */
	public Boete berekenBoete(BoeteParameters params)
	{
		int overtreding = Math.abs(params.getSnelheid() - params.getSnelheidsLimiet());
		BoeteRegel regel = boeteRegelDAO.zoekRegelVoorSnelheid(overtreding);
		if (regel == null)
			throw new RuntimeException("Kan geen boete bepalen voor snelheid "
					+ params.getSnelheid());

		// Bereken de boete.
		Boete boete = berekenBedragMetRegel(params, regel);
		params.setBoete(boete);
		// Publiceer de boete in de afhandelingstopic.
		publiceerBoete(params);
		
		return boete;
	}

	private Boete berekenBedragMetRegel(BoeteParameters params, BoeteRegel regel)
	{
		// Initieel bedrag
		double bedrag = regel.getBedrag();
		// Als het snelheidsgebied <= 50 km/u is, volgt er een opslag van 100%
		if (params.getSnelheidsLimiet() <= 50)
			bedrag *= 2;
		// Het aantal zwaartepunten van het boetebedrag moet vermenigvuldigd
		// worden met 50%.
		int zwaartepunten = params.getZwaartepunten() + regel.getZwaartePunt();
		if (zwaartepunten > 5)
			bedrag += (zwaartepunten - 5) * (bedrag / 2);

		// Initialiseer boete.
		Boete boete = new Boete();
		boete.setBedrag(bedrag);
		boete.setBoeteZwaartePunt(regel.getZwaartePunt());
		return boete;
	}

	private void publiceerBoete(BoeteParameters boeteParameters)
	{
		ObjectMessage message = context.createObjectMessage(boeteParameters);
        context.createProducer().send(boeteAfhandelingTopic, message);
        System.err.println("Boete gepubliceerd naar topic");
	}

}
