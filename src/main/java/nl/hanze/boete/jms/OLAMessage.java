package nl.hanze.boete.jms;

import java.io.IOException;
import java.net.SocketException;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import nl.hanze.boete.BoeteParameters;
import nl.hanze.web.ola.OLAClient;

/**
 * Message-Driven Bean implementation class for: OLAMessage
 */
@MessageDriven(activationConfig =
	{
			@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
			@ActivationConfigProperty(propertyName = "destination", propertyValue = "boeteafhandeling") }, mappedName = "boeteafhandeling")
public class OLAMessage implements MessageListener
{
	private OLAClient olaClient = new OLAClient();

	/**
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message)
	{
		try
		{
			BoeteParameters boete = message.getBody(BoeteParameters.class);
			System.err.println(getClass().getSimpleName() + ": Boete ontvangen voor "
					+ boete.getNatuurlijkPersoon().getBsn() + " met bedrag "
					+ boete.getBoete().getBedrag());
			try
			{
				olaClient.stuurBoeteNaarInbox(boete);
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch (JMSException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
