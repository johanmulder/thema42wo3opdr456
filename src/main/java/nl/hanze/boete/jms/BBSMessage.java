package nl.hanze.boete.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import nl.hanze.boete.BoeteBepalingBean;
import nl.hanze.boete.BoeteParameters;

/**
 * Message-Driven Bean implementation class for: BBSMessage
 */
@MessageDriven(activationConfig =
	{
			@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
			@ActivationConfigProperty(propertyName = "destination", propertyValue = "boetebepaling") }, mappedName = "boetebepaling")
public class BBSMessage implements MessageListener
{
	@EJB
	private BoeteBepalingBean bbp;

	/**
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message)
	{
		try
		{
			BoeteParameters params = message.getBody(BoeteParameters.class);
			bbp.berekenBoete(params);
		}
		catch (JMSException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
