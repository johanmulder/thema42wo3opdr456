package nl.hanze.boete.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import nl.hanze.boete.BoeteParameters;
import nl.hanze.boete.dao.BoeteRegistratieDAO;
import nl.hanze.boete.dto.BoeteRegistratie;

/**
 * Message-Driven Bean implementation class for: BOSMessage
 */
@MessageDriven(activationConfig =
	{
			@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
			@ActivationConfigProperty(propertyName = "destination", propertyValue = "boeteafhandeling") }, mappedName = "boeteafhandeling")
public class BOSMessage implements MessageListener
{
	@Inject
	private BoeteRegistratieDAO boeteRegistratieDAO;

	/**
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message)
	{
		try
		{
			BoeteParameters boete = message.getBody(BoeteParameters.class);
			System.err.println(getClass().getSimpleName() + ": Boete ontvangen voor "
					+ boete.getNatuurlijkPersoon().getBsn() + " met bedrag "
					+ boete.getBoete().getBedrag());
			boeteRegistratieDAO.create(converteerBoeteParameters(boete));
		}
		catch (JMSException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Converteer een ontvangen BoeteParameters object naar een BoeteRegistratie
	 * object.
	 * 
	 * @param params
	 * @return
	 */
	private BoeteRegistratie converteerBoeteParameters(BoeteParameters params)
	{
		BoeteRegistratie b = new BoeteRegistratie();
		b.setBedrag(params.getBoete().getBedrag());
		b.setZwaartepunten(params.getBoete().getBoeteZwaartePunt());
		b.setBsn(params.getNatuurlijkPersoon().getBsn());
		b.setSnelheid(params.getSnelheid());
		b.setSnelheidsLimiet(params.getSnelheidsLimiet());
		b.setTijdstip(params.getTijdstip());
		return b;
	}

}
