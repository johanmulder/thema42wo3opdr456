package nl.hanze.boete.dao;

import nl.hanze.boete.dto.BoeteRegel;

public interface BoeteRegelDAO
{
	public BoeteRegel zoekRegelVoorSnelheid(int snelheid);
}
