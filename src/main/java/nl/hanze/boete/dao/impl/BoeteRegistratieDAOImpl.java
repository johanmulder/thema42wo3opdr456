package nl.hanze.boete.dao.impl;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import nl.hanze.boete.dao.BoeteRegistratieDAO;
import nl.hanze.boete.dto.BoeteRegistratie;

@RequestScoped
public class BoeteRegistratieDAOImpl implements BoeteRegistratieDAO
{
	@PersistenceContext(unitName = "BBS")
	private EntityManager em;

	@Override
	public void create(BoeteRegistratie boete)
	{
		em.persist(boete);
	}

}
