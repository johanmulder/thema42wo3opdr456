package nl.hanze.boete.dao.impl;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import nl.hanze.boete.dao.BoeteRegelDAO;
import nl.hanze.boete.dto.BoeteRegel;

@RequestScoped
public class BoeteRegelDAOImpl implements BoeteRegelDAO
{
	@PersistenceContext(unitName = "BBS")
	private EntityManager em;
	
	@Override
	public BoeteRegel zoekRegelVoorSnelheid(int snelheid)
	{
		String query = "SELECT b FROM BoeteRegel b WHERE :snelheid BETWEEN b.minimumSnelheid AND " +
				"b.maximumSnelheid";
		return em.createQuery(query, BoeteRegel.class)
				.setParameter("snelheid", snelheid)
				.getSingleResult();
	}

}
