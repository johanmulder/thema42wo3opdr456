package nl.hanze.boete.dao;

import nl.hanze.boete.dto.BoeteRegistratie;

public interface BoeteRegistratieDAO
{
	/**
	 * Registreer een nieuwe boete.
	 * @param boete
	 */
	public void create(BoeteRegistratie boete);
}
