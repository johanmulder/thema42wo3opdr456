package nl.hanze.boete.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "boete_registratie")
public class BoeteRegistratie implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "bsn")
	private long bsn;
	@Column(name = "tijdstip")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tijdstip;
	@Column(name = "bedrag")
	private double bedrag;
	@Column(name = "zwaartepunten")
	private int zwaartepunten;
	@Column(name = "snelheid")
	private int snelheid;
	@Column(name = "snelheidslimiet")
	private int snelheidsLimiet;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public long getBsn()
	{
		return bsn;
	}

	public void setBsn(long bsn)
	{
		this.bsn = bsn;
	}

	public Date getTijdstip()
	{
		return tijdstip;
	}

	public void setTijdstip(Date tijdstip)
	{
		this.tijdstip = tijdstip;
	}

	public double getBedrag()
	{
		return bedrag;
	}

	public void setBedrag(double bedrag)
	{
		this.bedrag = bedrag;
	}

	public int getZwaartepunten()
	{
		return zwaartepunten;
	}

	public void setZwaartepunten(int zwaartepunten)
	{
		this.zwaartepunten = zwaartepunten;
	}

	public int getSnelheid()
	{
		return snelheid;
	}

	public void setSnelheid(int snelheid)
	{
		this.snelheid = snelheid;
	}

	public int getSnelheidsLimiet()
	{
		return snelheidsLimiet;
	}

	public void setSnelheidsLimiet(int snelheidsLimiet)
	{
		this.snelheidsLimiet = snelheidsLimiet;
	}
}
