package nl.hanze.boete.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "boete_regel")
public class BoeteRegel
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "boete_regel_id")
	private int id;
	@Column(name = "snelheid_min")
	private int minimumSnelheid;
	@Column(name = "snelheid_max")
	private int maximumSnelheid;
	@Column(name = "bedrag")
	private double bedrag;
	@Column(name = "zwaartepunt")
	private int zwaartePunt;
	
	public BoeteRegel()
	{
	}
	
	/**
	 * Constructor voor junit test.
	 * @param bedrag
	 * @param zwaartePunt
	 */
	public BoeteRegel(double bedrag, int zwaartePunt)
	{
		this.bedrag = bedrag;
		this.zwaartePunt = zwaartePunt;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getMinimumSnelheid()
	{
		return minimumSnelheid;
	}

	public void setMinimumSnelheid(int minimumSnelheid)
	{
		this.minimumSnelheid = minimumSnelheid;
	}

	public int getMaximumSnelheid()
	{
		return maximumSnelheid;
	}

	public void setMaximumSnelheid(int maximumSnelheid)
	{
		this.maximumSnelheid = maximumSnelheid;
	}

	public double getBedrag()
	{
		return bedrag;
	}

	public void setBedrag(double bedrag)
	{
		this.bedrag = bedrag;
	}

	public int getZwaartePunt()
	{
		return zwaartePunt;
	}

	public void setZwaartePunt(int zwaartePunt)
	{
		this.zwaartePunt = zwaartePunt;
	}
}
