package nl.hanze.web.ola;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class AcceptGiroRunner
{
	private String inputDir = null;
	private String outputDir = null;

	public AcceptGiroRunner()
	{
	}

	/**
	 * AcceptGiroRunner constructor.
	 * 
	 * @param inputDir De directory waar de acceptgiro bestanden uit worden
	 *            gelezen.
	 * @param outputDir De directory waar de pdf's naartoe worden geschreven.
	 */
	public AcceptGiroRunner(String inputDir, String outputDir)
	{
		this.inputDir = inputDir;
		this.outputDir = outputDir;
	}

	/**
	 * Start het acceptgiroverwerkingsproces met de inputDir en outputDir uit de
	 * class constructor.
	 */
	public void runProcess()
	{
		if (inputDir == null || outputDir == null)
			throw new IllegalArgumentException("Input dir of output dir niet gespecificeerd");
		runProcess(inputDir, outputDir);
	}

	/**
	 * Start het acceptgiroverwerkingsproces.
	 * 
	 * @param inputDir
	 * @param outputDir
	 */
	public void runProcess(String inputDir, String outputDir)
	{
		try
		{
			AcceptGiroProcessor processor = new AcceptGiroProcessor(inputDir, outputDir);
			processor.process();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void scheduleTask(long delay)
	{
		Timer timer = new Timer();
		timer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				runProcess();
			}
		}, 1000L);

	}

	public static void main(String[] args)
	{
		
		new AcceptGiroRunner("/Users/johan/workspaces/school42/thema42wo3opdr4/queue/input",
				"/Users/johan/workspaces/school42/thema42wo3opdr4/queue/output").runProcess();
	}
}
