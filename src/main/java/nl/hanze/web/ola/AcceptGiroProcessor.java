package nl.hanze.web.ola;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class AcceptGiroProcessor
{
	private final AcceptGiroFileReader fileReader;
	private final AcceptGiroValidator validator;
	private final AcceptGiroTransformer transformer;
	private final AcceptGiro pdfCreator;
	private final AcceptGiroFileRemover remover;
	private final String inputDir;
	
	public AcceptGiroProcessor(String inputDir, String outputDir)
	{
		fileReader = new AcceptGiroFileReader();
		validator = new AcceptGiroValidator();
		transformer = new AcceptGiroTransformer();
		pdfCreator = new AcceptGiro("/tmp", outputDir);
		remover = new AcceptGiroFileRemover();
		this.inputDir = inputDir;
	}
	
	public void process() throws IOException
	{
		Path dir = new File(inputDir).toPath();
		try (DirectoryStream<Path> dirContents = Files.newDirectoryStream(dir))
		{
			for (Path file : dirContents)
			{
				try
				{
					System.out.println(file.toString());
					processFile(file.toFile());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	private void processFile(File file) throws NumberFormatException, Exception
	{
		// Lees het bestand.
		Map<String, String> fields = fileReader.readFile(file);
		// Valideer het.
		validator.validate(fields);
		// Maak de PDF.
		pdfCreator.createAcceptGiroPdf(Long.parseLong(fields.get("REFERENCE")), 
				transformer.getEuros(fields.get("BEDRAG")), 
				transformer.getCenten(fields.get("BEDRAG")),
				transformer.formatteerBetalingsKenmerk(fields.get("BETALINGSKENMERK")),
				nullString(fields.get("REKENINGNUMMER")), 
				transformer.formatteerNaam(fields.get("GESLACHT"), fields.get("INIT"), fields.get("ACHTERNAAM")),
				transformer.formatteerAdres(fields.get("STRAATNAAM"), fields.get("STRAATNUMMER"), fields.get("POSTCODE")), 
				fields.get("PLAATSNAAM"), 
				fields.get("REKENINGNUMMERNAAR"),
				"CJIB");
		// Verwijder de PDF.
		remover.removeFile(file);
	}

	/**
	 * Converteer string naar "" indien deze null is.
	 * @param s
	 * @return
	 */
	private String nullString(String s)
	{
		if (s == null)
			return "";
		return s;
	}
}
