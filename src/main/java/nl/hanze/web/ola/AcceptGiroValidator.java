package nl.hanze.web.ola;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import javax.validation.ValidationException;

/**
 * 
 * @author Johan Mulder
 *
 */
@SuppressWarnings("unused")
public class AcceptGiroValidator
{
	private static Map<String, Method> validators;

	static
	{
		try
		{
			validators = new HashMap<>();
			validators.put("BEDRAG",
					AcceptGiroValidator.class.getDeclaredMethod("valideerBedrag", String.class));
			validators.put("BETALINGSKENMERK", AcceptGiroValidator.class.getDeclaredMethod(
					"valideerBetalingsKenmerk", String.class));
			validators.put("REKENINGNUMMER", AcceptGiroValidator.class.getDeclaredMethod(
					"valideerRekeningNummer", String.class));
			validators.put("INIT",
					AcceptGiroValidator.class.getDeclaredMethod("valideerInitialen", String.class));
			validators.put("ACHTERNAAM",
					AcceptGiroValidator.class.getDeclaredMethod("valideerNaam", String.class));
			validators.put("STRAATNAAM",
					AcceptGiroValidator.class.getDeclaredMethod("valideerNaam", String.class));
			validators.put("STRAATNUMMER",
					AcceptGiroValidator.class.getDeclaredMethod("valideerNummer", String.class));
			validators.put("POSTCODE",
					AcceptGiroValidator.class.getDeclaredMethod("valideerPostcode", String.class));
			validators.put("PLAATSNAAM",
					AcceptGiroValidator.class.getDeclaredMethod("valideerNaam", String.class));
			validators.put("REKENINGNUMMERNAAR", AcceptGiroValidator.class.getDeclaredMethod(
					"valideerRekeningNummerNaar", String.class));
		}
		catch (NoSuchMethodException | SecurityException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Valideer een map van acceptgiro regels.
	 * @param fields
	 * @throws ValidationException
	 */
	public void validate(Map<String, String> fields) throws ValidationException
	{
		for (String field : fields.keySet())
			if (validators.containsKey(field))
				doValidate(fields.get(field), validators.get(field));
	}
	
	private void doValidate(String value, Method validator)
	{
		try
		{
			validator.invoke(null, value);
		}
		catch (InvocationTargetException e)
		{
			if (e.getCause() instanceof ValidationException)
				throw (ValidationException) e.getCause();
			throw new RuntimeException(e);
		}
		catch (IllegalAccessException | IllegalArgumentException e)
		{
			throw new RuntimeException(e);
		}
		
	}

	/**
	 * Valideer een int tussen een minimale en maximale waarde.
	 * @param i
	 * @param min
	 * @param max
	 */
	private static void validateInt(int i, int min, int max)
	{
		if (i < min || i > max)
			throw new ValidationException("Nummer " + i + " valt niet tussen " + min + " en " + max);
	}
	
	private static void valideerBedrag(String bedrag) throws ValidationException
	{
		String[] a = bedrag.split(",");
		if (a.length != 2)
			throw new ValidationException("Ongeldig bedrag");
		validateInt(Integer.parseInt(a[0]), 1, 9999);
		validateInt(Integer.parseInt(a[1]), 0, 99);
	}

	private static void valideerRekeningNummer(String rekeningNummer) throws ValidationException
	{
		if (rekeningNummer.trim().length() == 0)
			return;
		valideerRekeningNummerNaar(rekeningNummer);
	}

	private static void valideerBetalingsKenmerk(String kenmerk) throws ValidationException
	{
		if (!kenmerk.matches("^[0-9]{16}$"))
			throw new ValidationException("Betalingskenmerk valideert niet");
	}

	private static void valideerInitialen(String initialen) throws ValidationException
	{
		if (!initialen.matches("^[a-zA-Z]{1,5}$"))
			throw new ValidationException("Initialen valideren niet");
	}

	private static void valideerNaam(String naam) throws ValidationException
	{
		if (!naam.matches("^[a-zA-Z ]{1,20}$"))
			throw new ValidationException("Naam valideert niet");
	}

	private static void valideerNummer(String nummer) throws ValidationException
	{
		validateInt(Integer.parseInt(nummer), 1, 9999);
	}

	private static void valideerPostcode(String postcode) throws ValidationException
	{
		if (!postcode.matches("^[0-9]{4}[A-Z]{2}$"))
			throw new ValidationException("Naam valideert niet");
	}

	private static void valideerRekeningNummerNaar(String rekeningNummer) throws ValidationException
	{
		if (!rekeningNummer.matches("^[0-9]{1,10}$"))
			throw new ValidationException("Rekeningnummer match niet met ^[0-9]{1,10}$");
	}
}
