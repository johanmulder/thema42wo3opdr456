package nl.hanze.web.ola;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import nl.hanze.boete.BoeteParameters;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;

public class OLAClient
{
	private AcceptGiroValidator validator = new AcceptGiroValidator();
	// FTP configuratie
	private String username;
	private String password;
	private String serverName;
	private int serverPort;
	private String uploadDir;
	
	public OLAClient()
	{
		Properties props = new Properties();
		try
		{
			props.load(OLAClient.class.getResourceAsStream("ftp.properties"));
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		
		username = props.getProperty("ftp.username");
		password = props.getProperty("ftp.password");
		serverName = props.getProperty("ftp.server.name");
		serverPort = Integer.parseInt(props.getProperty("ftp.server.port"));
		uploadDir = props.getProperty("ftp.dir.upload");
	}

	public void stuurBoeteNaarInbox(BoeteParameters boete) throws SocketException, IOException
	{
		Map<String, String> fields = converteerBoeteNaarOLA(boete);
		validator.validate(fields);
		uploadFile(fields);
	}

	private void uploadFile(Map<String, String> fields) throws IOException
	{
		FTPClient ftp = new FTPClient();
		try
		{
			FTPClientConfig config = new FTPClientConfig();
			ftp.configure(config);
	
			ftp.connect(serverName, serverPort);
			ftp.login(username, password);
	
			ftp.changeWorkingDirectory(uploadDir);
			InputStream olaInput = new ByteArrayInputStream(fieldsNaarString(fields).getBytes());
			ftp.storeFile(fields.get("REFERENCE"), olaInput);
			olaInput.close();
		}
		finally
		{
			if (ftp != null && ftp.isConnected())
				ftp.disconnect();
		}
	}
	
	private String fieldsNaarString(Map<String, String> fields)
	{
		StringBuffer sb = new StringBuffer();
		for (String key : fields.keySet())
			sb.append(key + "=" + fields.get(key) + "\n");
		return sb.toString();
	}

	private Map<String, String> converteerBoeteNaarOLA(BoeteParameters boete)
	{
		Map<String, String> fields = new LinkedHashMap<>();
		fields.put("REFERENCE", Long.toString(System.currentTimeMillis()));
		fields.put("BEDRAG", getBedrag(boete.getBoete().getBedrag()));
		fields.put("BETALINGSKENMERK", getBetalingsKenmerk());
		fields.put("REKENINGNUMMER", "");
		NatuurlijkPersoon np = boete.getNatuurlijkPersoon();
		fields.put("GESLACHT", Character.toString(np.getGeslacht()));
		fields.put("INIT", np.getInitialen());
		fields.put("ACHTERNAAM", np.getAchternaam());
		fields.put("STRAATNAAM", np.getStraatnaam());
		fields.put("STRAATNUMMER", Integer.toString(np.getNummer()));
		fields.put("POSTCODE", np.getPostcode());
		fields.put("PLAATSNAAM", np.getWoonplaats());
		fields.put("REKENINGNUMMERNAAR", "123456789");
		return fields;
	}

	private String getBetalingsKenmerk()
	{
		StringBuffer b = new StringBuffer();
		Random r = new Random();
		for (int i = 0; i < 4; i++)
			b.append(String.format("%04d", r.nextInt(10000)));
		System.err.println(b.toString());
		return b.toString();
	}

	private String getBedrag(double bedrag)
	{
		String[] parts = Double.toString(bedrag).split("\\.");
		return parts[0] + "," + String.format("%02d", Integer.parseInt(parts[1]));
	}
}
