package nl.hanze.web.ola;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author Johan Mulder
 *
 */
public class AcceptGiroFileReader
{
	/**
	 * Lees een bestand en sla het resultaat op in een map. Er wordt aangenomen dat alle
	 * regels in vorm "KEY=value" zijn.
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Map<String, String> readFile(File file) throws FileNotFoundException, IOException
	{
		// Gebruik een LinkedHashMap om de volgorde uit het bestand aan te houden.
		Map<String, String> fields = new LinkedHashMap<>();
		// Gebruik een auto closable FileReader.
		try (FileReader fileReader = new FileReader(file))
		{
			readLines(fileReader, fields);
		}
		return fields;
	}
	
	/**
	 * Lees de regels uit een FileReader en voeg het resultaat toe aan fields.  
	 * @param fileReader
	 * @param fields
	 * @throws IOException
	 */
	public void readLines(FileReader fileReader, Map<String, String> fields) throws IOException
	{
		// Gebruik een auto closable buffered reader.
		try (BufferedReader in = new BufferedReader(fileReader))
		{
			String line;
			while ((line = in.readLine()) != null)
			{
				String[] lineFields = line.split("=");
				if (lineFields.length != 2)
					continue;
				fields.put(lineFields[0].trim(), lineFields[1].trim());
			}
		}
	}
}
