package nl.hanze.web.ola;

public class AcceptGiroTransformer
{
	private int getStringField(String s, String separator, int fieldNum)
	{
		String[] fields = s.split(separator);
		return Integer.parseInt(fields[fieldNum]);
	}

	/**
	 * Haal euros uit het gegeven bedrag.
	 * 
	 * @param bedrag
	 * @return
	 */
	public int getEuros(String bedrag)
	{
		return getStringField(bedrag, ",", 0);
	}

	/**
	 * Haal centen uit het gegeven bedrag.
	 * 
	 * @param bedrag
	 * @return
	 */
	public int getCenten(String bedrag)
	{
		return getStringField(bedrag, ",", 1);
	}

	/**
	 * Formatteer een betalingskenmerk.
	 * 
	 * @param kenmerk
	 * @return
	 */
	public String formatteerBetalingsKenmerk(String kenmerk)
	{
		return kenmerk.substring(0, 4) + " " + kenmerk.substring(4, 8) + " "
				+ kenmerk.substring(8, 12) + " " + kenmerk.substring(12);
	}

	/**
	 * Formatteer een naam
	 * 
	 * @param geslacht
	 * @param initialen
	 * @param achternaam
	 * @return
	 */
	public String formatteerNaam(String geslacht, String initialen, String achternaam)
	{
		return (geslacht.equals("M") ? "Dhr" : "Mevr") + " "
				+ (initialen == null ? "" : initialen + " ") + achternaam;
	}

	/**
	 * Formatteer een adres.
	 * 
	 * @param straatnaam
	 * @param huisNr
	 * @param postcode
	 * @return
	 */
	public String formatteerAdres(String straatnaam, String huisNr, String postcode)
	{
		return straatnaam + " " + huisNr + " " + postcode;
	}

}
