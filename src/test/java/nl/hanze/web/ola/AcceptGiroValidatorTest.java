package nl.hanze.web.ola;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class AcceptGiroValidatorTest
{
	private AcceptGiroValidator validator;

	@Before
	public void setUp() throws Exception
	{
		validator = new AcceptGiroValidator();
	}

	@Test
	public void test()
	{
		Map<String, String> fields = new HashMap<>();
		fields.put("REFERENCE", "123");
		fields.put("BEDRAG", "123,23");
		fields.put("BETALINGSKENMERK", "1234123412341234");
		fields.put("REKENINGNUMMER", "1");
		fields.put("GESLACHT", "M");
		fields.put("INIT", "T");
		fields.put("ACHTERNAAM", "Tester");
		fields.put("STRAATNAAM", "Teststraat");
		fields.put("STRAATNUMMER", "12");
		fields.put("POSTCODE", "1234AB");
		fields.put("PLAATSNAAM", "Testerdam");
		fields.put("REKENINGNUMMERNAAR", "123456789");
		validator.validate(fields);
	}

}
