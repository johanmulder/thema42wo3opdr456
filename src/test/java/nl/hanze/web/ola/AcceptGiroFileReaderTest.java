package nl.hanze.web.ola;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class AcceptGiroFileReaderTest
{
	private AcceptGiroFileReader reader;

	@Before
	public void setUp() throws Exception
	{
		reader = new AcceptGiroFileReader();
	}

	@Test
	public void test() throws FileNotFoundException, IOException, URISyntaxException
	{
		File file = new File(AcceptGiroFileReaderTest.class.getResource("testfile.txt").toURI());
		Map<String, String> lines = reader.readFile(file);
		assertEquals(3, lines.size());
		assertEquals(lines.get("TEST1"), "test1");
		assertEquals(lines.get("TEST2"), "test2");
		assertEquals(lines.get("TEST3"), "test3");
	}

}
