package nl.hanze.boete;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.Date;

import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;

import nl.hanze.boete.dao.BoeteRegelDAO;
import nl.hanze.boete.dto.BoeteRegel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class BoeteBepalingBeanTest
{
	private BoeteRegelDAO boeteRegelDAO;
	private BoeteBepalingBean bbpBean;
	private JMSContext context;
	private JMSProducer jmsProducer;
	

	@Before
	public void setUp()
	{
		boeteRegelDAO = Mockito.mock(BoeteRegelDAO.class);
		bbpBean = new BoeteBepalingBean();
		bbpBean.setBoeteRegelDAO(boeteRegelDAO);
		context = Mockito.mock(JMSContext.class);
		jmsProducer = Mockito.mock(JMSProducer.class, new Answer() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable
			{
				// TODO Auto-generated method stub
				return null;
			}
			
		});
		
		when(context.createProducer()).thenReturn(jmsProducer);
		bbpBean.setJMSContext(context);
	}

	@Test
	public void testBerekenBoete1()
	{
		// Situatie 1: boetedoener rijdt 54 in een 50-gebied met 0
		// zwaartepunten. Resultaat is €50, vanwege de verdubbeling door een
		// overtreding in een 50 km/u gebied
		testBerekenBoeteGeneriek(new BoeteParameters(54, 50, 0, null, new Date()), new BoeteRegel(25,
				1), 50);
	}

	@Test
	public void testBerekenBoete2()
	{
		// Situatie 2: boetedoener rijdt 152 in een 120-gebied met 1
		// zwaartepunt. Resultaat is €250, want verder geen zwaartepunten.
		testBerekenBoeteGeneriek(new BoeteParameters(152, 120, 1, null, new Date()), new BoeteRegel(
				250, 4), 250);
	}

	@Test
	public void testBerekenBoete3()
	{
		// Situatie 3: boetedoener rijdt 75 in een 30-gebied met 4
		// zwaartepunten. Resultaat moet €2400 zijn, omdat het totaal aantal
		// zwaartepunten op 9 komt (4x50% van €400)
		testBerekenBoeteGeneriek(new BoeteParameters(75, 30, 4, null, new Date()), new BoeteRegel(
				400.0, 5), 2400.0);
	}

	/**
	 * Bereken boete aan de hand van generieke parameters en een dummy regel.
	 * 
	 * @param params
	 * @param dummyRegel
	 * @param verwachtBedrag
	 */
	private void testBerekenBoeteGeneriek(BoeteParameters params, BoeteRegel dummyRegel,
			double verwachtBedrag)
	{
		int overtreding = Math.abs(params.getSnelheid() - params.getSnelheidsLimiet());
		when(boeteRegelDAO.zoekRegelVoorSnelheid(eq(overtreding))).thenReturn(dummyRegel);
		when(context.createObjectMessage()).thenReturn(null);
		// Bereken boete.
		Boete boete = bbpBean.berekenBoete(params);
		// Test uitkomst.
		assertNotNull(boete);
		assertEquals(boete.getBoeteZwaartePunt(), dummyRegel.getZwaartePunt());
		assertEquals(boete.getBedrag(), verwachtBedrag, 0.1);
	}
}
